module.exports = {
    "extends": ["standard"],
    "parser": "babel-eslint",
    "plugins": [
        "babel"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true,
            "modules": true
        }
    },
    "env": {
        "browser": true,
        "amd": true,
        "es6": true,
        "node": true
    },
    "rules": {
        "react/no-unescaped-entities": 0,
        "no-unused-vars": 1,
        "no-tabs": 0,
        "indent": [2]
    },
    "globals": {
        "i18n": true
    }
};


