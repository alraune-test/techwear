import $ from 'jquery'

$(document).ready(function () {
  let pgurl = window.location.href.substr(window.location.href.lastIndexOf('/') + 1)
  $('.main-nav a').each(function () {
    if ($(this).attr('href') === pgurl) {
      $(this).addClass('active')
    }
  })
})
