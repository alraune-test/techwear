import _ from 'lodash'

export default class Helper {
  static menuOpen (menu, cls) {
    let classList = menu.classList.value
    menu.setAttribute('class', `${classList} ${cls}`)
  }

  static menuClose (menu, cls) {
    menu.classList.remove(`${cls}`)
  }

  static menusClose (cls) {
    _.map(document.getElementsByClassName(cls), (item) => {
      Helper.menuClose(item, cls)
    })
  }

  static documentScroll () {
    document.body.classList.add('js_no-scroll')
  }

  static documentNoScroll () {
    document.body.classList.remove('js_no-scroll')
  }
}
