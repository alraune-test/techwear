import $ from 'jquery'
import _ from 'lodash'
import Header from '../Layout/Header'

export default class DOMHelper {
  static findAncestor (el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el
  }

  static findChild (el, cls) {
    let childElem = null
    _.map(el.children, (child) => {
      if (this.hasClass(child, cls)) {
        childElem = child
      }
    })
    return childElem
  }

  static mediaListener (value, success, error) {
    try {
      if (value.matches) {
        success()
      } else {
        error()
      }
    } catch (err) {
      let txt = 'There was an error on this page.\n\n'
      txt += 'Error description: ' + err + '\n\n'
      console.log(txt)
    }
  }

  static getDataAttr (elem) {
    return elem.dataset.action
  }

  static hasClass (element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1
  }

  static sticky (elems) {
    _.map(elems, (elem) => {
      if (document.getElementsByClassName(elem)) {
        let scrollTop = $('html body').scrollTop()
        let elementOffset = $(elem).offset().top
        let distance = (elementOffset - scrollTop - Header.getProps().nav.offsetHeight)
        window.onscroll = () => {
          let body = document.getElementsByTagName('body')[0]
          // let sticky = nav.offsetTop
          let screenResolution = window.matchMedia('(max-width: 1024px)')
          let success = () => {
            if (window.pageYOffset >= distance) {
              body.classList.add('js_sticky')
            } else {
              body.classList.remove('js_sticky')
            }
          }
          let error = () => {
            body.classList.remove('js_sticky')
          }
          this.mediaListener(screenResolution, success, error)
          screenResolution.addListener(this.mediaListener)
        }
      }
    })
  }

  static moveElem (elem, parent, nextSibling) {
    parent.insertBefore(elem, nextSibling)
  }

  static removeElem (elem, parent) {
    parent.removeChild(elem)
  }

  static createElem (elem, cls, parent, nextSibling) {
    let newNode = document.createElement(elem)
    newNode.className = cls
    this.moveElem(newNode, parent, nextSibling)
  }
}
