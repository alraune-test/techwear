import $ from 'jquery'

$(document).ready(function ($) {
  let cartWrapper = $('.cd-cart-container')
  // product id - you don't need a counter in your real project but you can use your real product id
  let productId = 0

  if (cartWrapper.length > 0) {
    // store $ objects
    var cartBody = cartWrapper.find('.body')
    var cartList = cartBody.find('ul')
    var cartTotal = cartWrapper.find('.checkout').find('span')
    var cartTrigger = cartWrapper.children('.cd-cart-trigger')
    var cartCount = $('.cd-cart-container').find('.count')
    var addToCartBtn = $('.cd-add-to-cart')
    var undo = cartWrapper.find('.undo')
    var undoTimeoutId

    // add product to cart
    addToCartBtn.on('click', function (event) {
      event.preventDefault()
      var currentProduct = event.target.closest('.product-item')
      addToCart($(this), currentProduct)
    })

    // open/close cart
    cartTrigger.on('click', function (event) {
      event.preventDefault()
      toggleCart()
    })

    // close cart when clicking on the .cd-cart-container::before (bg layer)
    cartWrapper.on('click', function (event) {
      if ($(event.target).is($(this))) toggleCart(true)
    })

    // delete an item from the cart
    cartList.on('click', '.delete-item', function (event) {
      event.preventDefault()
      removeProduct($(event.target).parents('.product'))
    })

    $(document).on('click', '.quantity .control-count', function () {
      if ($(this).hasClass('control-decr')) {
        if (Number($(this).closest('.quantity').find('.quantity-value').val()) !== 1) {
          $(this).closest('.quantity').find('.quantity-value').val(Number($(this).closest('.quantity').find('.quantity-value').val()) - 1)
          quickUpdateCart()
        }
      } else {
        $(this).closest('.quantity').find('.quantity-value').val(Number($(this).closest('.quantity').find('.quantity-value').val()) + 1)
        quickUpdateCart()
      }
    })

    // update item quantity
    // cartList.on('change', '.quantity .quantity-value', function (event) {
    //   quickUpdateCart()
    // })

    // reinsert item deleted from the cart
    undo.on('click', 'a', function (event) {
      clearInterval(undoTimeoutId)
      event.preventDefault()
      cartList.find('.deleted').addClass('undo-deleted').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
        $(this).off('webkitAnimationEnd oanimationend msAnimationEnd animationend').removeClass('deleted undo-deleted').removeAttr('style')
        quickUpdateCart()
      })
      undo.removeClass('visible')
    })
  }

  function toggleCart (bool) {
    let cartIsOpen = (typeof bool === 'undefined') ? cartWrapper.hasClass('cart-open') : bool

    if (cartIsOpen) {
      cartWrapper.removeClass('cart-open')
      // reset undo
      clearInterval(undoTimeoutId)
      undo.removeClass('visible')
      cartList.find('.deleted').remove()

      setTimeout(function () {
        cartBody.scrollTop(0)
        // check if cart empty to hide it
        if (Number(cartCount.find('li.first span').text()) === 0) cartWrapper.addClass('empty')
      }, 500)
    } else {
      cartWrapper.addClass('cart-open')
    }
  }

  function addToCart (trigger, product) {
    let cartIsEmpty = cartWrapper.hasClass('empty')
    // update cart product list
    addProduct(product)
    // update number of items
    updateCartCount(cartIsEmpty)
    // update total price
    updateCartTotal(trigger.data('price'), true)
    // show cart
    cartWrapper.removeClass('empty')
  }

  function addProduct (product) {
    product = $(product)
    // this is just a product placeholder
    // you should insert an item with the selected product info
    // replace productId, productName, price and url with your real product info
    productId = productId + 1
    let productAdded = $(`
        <li class="product">
            <div class="product-image">
                <a href="#0">${product.find('.product-image img')[0].outerHTML}</a>
            </div>
            <div class="product-details">
                <div class="details-header">
                  <h3><a href="#0">${product.find('.product-name')[0].outerHTML}</a></h3>
                  <span class="price">${product.find('.product-price')[0].outerHTML}</span>
                </div>
                <span class="size">Size: <strong>50/M</strong></span>
                <div class="actions">
                    <div class="quantity">
                        <span>Quantity:</span>
                        <span class="control-count control-decr"></span>
                        <input id="cd-product-${productId}" type="text" class="quantity-value" value="1" disabled>
                        <span class="control-count control-incr"></span>
                    </div>
                    <a href="#0" class="delete-item"></a>
                </div>
            </div>
        </li>`)
    cartList.prepend(productAdded)
  }

  function removeProduct (product) {
    clearInterval(undoTimeoutId)
    cartList.find('.deleted').remove()

    let topPosition = product.offset().top - cartBody.children('ul').offset().top
    let productQuantity = Number(product.find('.quantity').find('.quantity-value').val())
    let productTotPrice = Number(product.find('.price').text().replace('$', '')) * productQuantity
    console.log(product)
    product.css('top', topPosition + 'px').addClass('deleted')

    // update items count + total price
    updateCartTotal(productTotPrice, false)
    updateCartCount(true, -productQuantity)
    undo.addClass('visible')

    // wait 8sec before completely remove the item
    undoTimeoutId = setTimeout(function () {
      undo.removeClass('visible')
      cartList.find('.deleted').remove()
    }, 8000)
  }

  function quickUpdateCart () {
    let quantity = 0
    let price = 0

    cartList.children('li:not(.deleted)').each(function () {
      let singleQuantity = Number($(this).find('.quantity .quantity-value').val())
      quantity = quantity + singleQuantity
      price = price + singleQuantity * Number($(this).find('.price').text().replace('$', ''))
    })

    cartTotal.text(price.toFixed(2))
    cartCount.find('li.first span').text(quantity)
    cartCount.find('li.second span').text(quantity + 1)
  }

  function updateCartCount (emptyCart, quantity) {
    if (typeof quantity === 'undefined') {
      let actual = Number(cartCount.find('li span').eq(0).text()) + 1
      let next = actual + 1

      if (emptyCart) {
        cartCount.find('li.first span').text(actual)
        cartCount.find('li.second span').text(next)
      } else {
        cartCount.addClass('update-count')
        setTimeout(function () {
          cartCount.find('li.first span').text(actual)
        }, 150)

        setTimeout(function () {
          cartCount.removeClass('update-count')
        }, 200)

        setTimeout(function () {
          cartCount.find('li.second span').text(next)
        }, 230)
      }
    } else {
      let actual = Number(cartCount.find('li span').eq(0).text()) + quantity
      let next = actual + 1

      cartCount.find('li.first span').text(actual)
      cartCount.find('li.second span').text(next)
    }
  }

  function updateCartTotal (price, bool) {
    bool ? cartTotal.text((Number(cartTotal.text()) + Number(price)).toFixed(2)) : cartTotal.text((Number(cartTotal.text()) - Number(price)).toFixed(2))
  }
})
