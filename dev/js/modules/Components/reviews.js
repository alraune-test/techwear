import $ from 'jquery'
import 'slick-carousel'

$(document).ready(function () {
  $('.reviews-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    infinite: false
  })
  $('.slider-pagination .left-btn').click(function () {
    $('.reviews-slider').slick('slickPrev')
  })

  $('.slider-pagination .right-btn').click(function () {
    $('.reviews-slider').slick('slickNext')
  })
  $('.slider-pagination .left-btn').hide()
  let $status = $('.slider-pagination .page-info')

  $('.reviews-slider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    let i = (currentSlide ? currentSlide : 0) + 1
    $status.text(i + '/' + slick.slideCount)
    if (i === 1) $('.slider-pagination .left-btn').hide()
    else $('.slider-pagination .left-btn').show()

    if (i === 3) $('.slider-pagination .right-btn').hide()
    else $('.slider-pagination .right-btn').show()
  })
})
