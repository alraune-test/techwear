import $ from 'jquery'
import 'slick-carousel'

$(document).ready(function () {
  $('.related-items-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    arrows: true
  })
  $('.our-technology-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  })
})
