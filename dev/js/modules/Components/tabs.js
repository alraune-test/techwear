import $ from 'jquery'
import tabs from 'tabs'

$(document).ready(function () {
  let container = document.querySelector('.tab-container')
  if (container) {
    tabs(container)
  }
})
