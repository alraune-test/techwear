import $ from 'jquery'
import wNumb from 'wnumb'
import noUiSlider from 'nouislider'

$(document).ready(function () {
  let keypressSlider = document.getElementById('keypress')
  let input0 = document.getElementById('input-with-keypress-0')
  let input1 = document.getElementById('input-with-keypress-1')
  let inputs = [input0, input1]

  if (keypressSlider && input0 && input1) {
    noUiSlider.create(keypressSlider, {
      start: [720, 2400],
      connect: true,
      range: {
        'min': 0,
        'max': 4000
      }
    })

    keypressSlider.noUiSlider.on('update', function (values, handle) {
      inputs[handle].value = values[handle]
    })
  }
  $('.item-select').on('click', function () {
    $(this).toggleClass('active')
  })
  $('.item-select-one').on('click', function () {
    $(this).closest('.selects').find('.active').removeClass('active')
    $(this).toggleClass('active')
  })
})
