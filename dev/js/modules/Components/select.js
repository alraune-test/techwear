import $ from 'jquery'
function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = null != arguments[i] ? arguments[i] : {}, ownKeys = Object.keys(source);
    "function" == typeof Object.getOwnPropertySymbols && (ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
      return Object.getOwnPropertyDescriptor(source, sym).enumerable
    }))), ownKeys.forEach(function (key) {
      _defineProperty(target, key, source[key])
    })
  }
  return target
}

function _defineProperty(obj, key, value) {
  return key in obj ? Object.defineProperty(obj, key, {
    value: value,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : obj[key] = value, obj
}

var CustomSelect = function ($) {
  var defaults = {
    block: "custom-select",
    hideCallback: !1,
    includeValue: !1,
    keyboard: !0,
    modifier: !1,
    placeholder: !1,
    search: !1,
    showCallback: !1,
    transition: 0
  }, CustomSelect = function () {
    function CustomSelect(select, options) {
      this._$select = $(select), this._options = _objectSpread({}, defaults, "object" == typeof options && options), this._activeModifier = this._options.block + "--active", this._dropupModifier = this._options.block + "--dropup", this._optionSelectedModifier = this._options.block + "__option--selected", this._keydown = this._keydown.bind(this), this._dropup = this._dropup.bind(this), this._outside = this._outside.bind(this), this._init()
    }

    var _proto = CustomSelect.prototype;
    return _proto.reset = function () {
      this._$dropdown.hide().empty(), this._fill()
    }, _proto._init = function () {
      this._$element = $('<div class="' + this._options.block + '">\n           <button class="' + this._options.block + "__option " + this._options.block + '__option--value" type="button"></button>\n           <div class="' + this._options.block + '__dropdown" style="display: none;"></div>\n         </div>'), this._$select.hide().after(this._$element), this._options.modifier && this._$element.addClass(this._options.modifier), this._$value = this._$element.find("." + this._options.block + "__option--value"), this._$dropdown = this._$element.find("." + this._options.block + "__dropdown"), this._fill()
    }, _proto._fill = function () {
      var _this = this;
      this._$values = this._$select.find("option"), this._values = [], $.each(this._$values, function (i, option) {
        var el = $(option).text().trim();
        _this._values.push(el)
      }), this._options.placeholder && (this._$select.find("[selected]").length ? this._options.placeholder = !1 : (this._$value.html(this._options.placeholder), this._$select.prop("selectedIndex", -1))), $.each(this._values, function (i, el) {
        var cssClass = _this._$values.eq(i).attr("class"),
            $option = $('<button class="' + _this._options.block + '__option" type="button">' + el + "</button>");
        el === _this._$select.find(":selected").text().trim() ? (_this._$value.text(el).addClass(cssClass).data("class", cssClass), (_this._options.includeValue || _this._options.placeholder) && ($option.addClass(cssClass), $option.addClass(_this._optionSelectedModifier), _this._$dropdown.append($option))) : ($option.addClass(cssClass), _this._$dropdown.append($option))
      }), this._$options = this._$dropdown.find("." + this._options.block + "__option"), this._options.search && this._search(), this._$value.one("click", function (event) {
        _this._show(event)
      }), this._$options.length || this._$value.prop("disabled", !0), this._$options.on("click", function (event) {
        _this._select(event)
      })
    }, _proto._show = function (event) {
      var _this2 = this;
      event.preventDefault(), this._dropup(), $(window).on("resize scroll", this._dropup), this._$element.addClass(this._activeModifier), this._$dropdown.slideDown(this._options.transition, function () {
        _this2._options.search && (_this2._$input.focus(), _this2._options.includeValue && _this2._scroll()), "function" == typeof _this2._options.showCallback && _this2._options.showCallback.call(_this2._$element[0])
      }), setTimeout(function () {
        var outsideClickEvent = "ontouchstart" in document.documentElement ? "touchstart" : "click";
        $(window).on(outsideClickEvent, _this2._outside)
      }, 0), this._$value.one("click", function (event) {
        event.preventDefault(), _this2._hide()
      }), this._options.keyboard && (this._options.index = -1, $(window).on("keydown", this._keydown))
    }, _proto._hide = function () {
      var _this3 = this;
      this._options.search && (this._$input.val("").blur(), this._$options.show(), this._$wrap.scrollTop(0)), this._$dropdown.slideUp(this._options.transition, function () {
        _this3._$element.removeClass(_this3._activeModifier).removeClass(_this3._dropupModifier), "function" == typeof _this3._options.hideCallback && _this3._options.hideCallback.call(_this3._$element[0]), $(window).off("touchstart click", _this3._outside).off("resize scroll", _this3._dropup), _this3._$value.off("click").one("click", function (event) {
          _this3._show(event)
        })
      }), this._options.keyboard && (this._$options.blur(), $(window).off("keydown", this._keydown))
    }, _proto._scroll = function () {
      var _this4 = this;
      $.each(this._$options, function (i, option) {
        var $option = $(option);
        if ($option.text() === _this4._$value.text()) {
          var top = $option.position().top, center = _this4._$wrap.outerHeight() / 2 - $option.outerHeight() / 2;
          return center < top && _this4._$wrap.scrollTop(top - center), !1
        }
      })
    }, _proto._select = function (event) {
      var _this5 = this;
      event.preventDefault();
      var choice = $(event.currentTarget).text().trim(), values = this._values.concat();
      if (this._$value.text(choice).removeClass(this._$value.data("class")), this._$values.prop("selected", !1), $.each(values, function (i, el) {
        _this5._options.includeValue || el !== choice || values.splice(i, 1), $.each(_this5._$values, function (i, option) {
          var $option = $(option);
          if ($option.text().trim() === choice) {
            var cssClass = $option.attr("class");
            $option.prop("selected", !0), _this5._$value.addClass(cssClass).data("class", cssClass)
          }
        })
      }), this._hide(), this._options.includeValue) this._$options.removeClass(this._optionSelectedModifier), $.each(this._$options, function (i, option) {
        var $option = $(option);
        if ($option.text().trim() === choice) return $option.addClass(_this5._optionSelectedModifier), !1
      }); else {
        if (this._$options.length > values.length) {
          var last = this._$options.eq(values.length);
          last.remove(), this._$options = this._$options.not(last), this._$options.length || this._$value.prop("disabled", !0)
        }
        $.each(this._$options, function (i, option) {
          var $option = $(option);
          $option.text(values[i]), $option.attr("class", _this5._options.block + "__option"), $.each(_this5._$values, function () {
            var $this = $(this);
            $this.text().trim() === values[i] && $option.addClass($this.attr("class"))
          })
        })
      }
      void 0 !== event.originalEvent && this._$select.trigger("change")
    }, _proto._search = function () {
      var _this6 = this;
      this._$input = $('<input class="' + this._options.block + '__input" autocomplete="off">'), this._$dropdown.prepend(this._$input), this._$options.wrapAll('<div class="' + this._options.block + '__option-wrap"></div>'), this._$wrap = this._$element.find("." + this._options.block + "__option-wrap"), this._$input.on("focus", function () {
        _this6._options.index = -1
      }), this._$input.on("keyup", function () {
        var query = _this6._$input.val().trim();
        query.length ? (_this6._$wrap.scrollTop(0), setTimeout(function () {
          query === _this6._$input.val().trim() && $.each(_this6._$options, function (i, option) {
            var $option = $(option), match = -1 !== $option.text().trim().toLowerCase().indexOf(query.toLowerCase());
            $option.toggle(match)
          })
        }, 300)) : _this6._$options.show()
      })
    }, _proto._dropup = function () {
      var bottom = this._$element[0].getBoundingClientRect().bottom,
          up = $(window).height() - bottom < this._$dropdown.height();
      this._$element.toggleClass(this._dropupModifier, up)
    }, _proto._outside = function (event) {
      var $target = $(event.target);
      $target.parents().is(this._$element) || $target.is(this._$element) || this._hide()
    }, _proto._keydown = function (event) {
      var $visible = this._$options.filter(":visible");
      switch (event.keyCode) {
        case 40:
          event.preventDefault(), $visible.eq(this._options.index + 1).length ? this._options.index += 1 : this._options.index = 0, $visible.eq(this._options.index).focus();
          break;
        case 38:
          event.preventDefault(), $visible.eq(this._options.index - 1).length && 0 <= this._options.index - 1 ? this._options.index -= 1 : this._options.index = $visible.length - 1, $visible.eq(this._options.index).focus();
          break;
        case 13:
        case 32:
          if (!this._$input || !this._$input.is(":focus")) {
            event.preventDefault();
            var $option = this._$options.add(this._$value).filter(":focus");
            $option.trigger("click"), $option.is(this._$value) || this._$select.trigger("change"), this._$value.focus()
          }
          break;
        case 27:
          event.preventDefault(), this._hide(), this._$value.focus()
      }
    }, CustomSelect._jQueryPlugin = function (config) {
      return this.each(function () {
        var $this = $(this), data = $this.data("custom-select");
        data ? "reset" === config && data.reset() : "string" != typeof config && (data = new CustomSelect(this, config), $this.data("custom-select", data))
      })
    }, CustomSelect
  }();
  return $.fn.customSelect = CustomSelect._jQueryPlugin, $.fn.customSelect.noConflict = function () {
    return $.fn.customSelect
  }, CustomSelect
}($);
//# sourceMappingURL=jquery.custom-select.min.js.map

$(document).ready(function () {
  $('select').customSelect()
})
