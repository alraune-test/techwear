// @Import js
import { $, jQuery } from 'jquery'
import './modules/Components/price-slider'
import './modules/page-active'
import './modules/Components/ProductGallery'
import './modules/Components/tabs'
import './modules/Components/related-items'
import './modules/Components/reviews'
import './modules/Components/search'
import './modules/Components/select'
import './modules/Components/cart'
import './modules/Components/control'
// eslint-disable-next-line
window.jQuery = require('jquery')
require('bootstrap3')

// @Events
document.addEventListener('DOMContentLoaded', () => {

  // @Start call function

  // @End call function;
})
