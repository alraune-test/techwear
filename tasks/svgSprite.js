/**
 * Build svg sprite
 */
const gulp = require('gulp')
const { reload } = require('browser-sync')
const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const svgSprite = require('gulp-svg-sprite')
const path = require('path')
const appRoot = require('app-root-path')
const appRootBase = appRoot.path

const config = require('./config')

const svgConf = {
  shape: {
    spacing: {
      padding: 0
    },
    dimension: {
      maxWidth: 32,
      maxHeight: 32
    }
  },
  mode: {
    css: {
      dimensions: true,
      common: 'icon',
      layout: 'vertical',
      prefix: '.icon-',
      bust: false,
      dest: './',
      sprite: 'dist/assets/images/svg/sprite.svg',
      render: {
        less: {
          /**
           * Change default CSS output path to ./dev/sass/vendor/_sprite.scss
           * Reason - to have just one bundle.css
           */
          dest: path.join(
            appRootBase,
            config.root.dev,
            config.css.dev,
            'template/vendor/sprite.less'
          ),
          template: path.join(
            appRootBase,
            config.root.dev,
            config.css.dev,
            'template/vendor/sprite-template.less'
          )
        }
      }
    }
  }
}

gulp.task('svg', () =>
  gulp
    .src(path.join(config.root.dev, config.svg.dev, '*.svg'))
    // .pipe(cheerio({
    //   run: function ($) {
    //     $('[fill]').removeAttr('fill')
    //     $('[stroke]').removeAttr('stroke')
    //     $('[style]').removeAttr('style')
    //   },
    //   parserOptions: {xmlMode: true}
    // }))
    // .pipe(replace('&gt;', '>'))
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(svgSprite(svgConf))
    // .on('end', function () {log(path.join(appRootBase, config.root.dev, config.css.dev, 'parts/vendor/_sprite.less'))})
    .pipe(gulp.dest('.'))
    .pipe(reload({ stream: true })))
