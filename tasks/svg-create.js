const gulp = require('gulp')
const path = require('path')
const fs = require('fs')
const appRoot = require('app-root-path')
const appRootBase = appRoot.path

const config = require('./config')

const pathToSprite = path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/sprite.svg')

gulp.task('svg-create', () => {
  let stream = fs.copyFile(pathToSprite, path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/main.svg'), (err) => {
    if (err) throw err
    console.log('sprite.svg was copied to main.svg')
  })
  // let stream = fs.readFile(pathToSprite, 'utf8', function (err, data) {
  //   if (err) {
  //     return console.log(err)
  //   }
  //   fs.stat(path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/main.svg'), function (err, stat) {
  //     if (err == null) {
  //       fs.unlink(path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/main.svg'))
  //     }
  //     fs.appendFile(path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/main.svg'), data, 'utf8', function (err) {
  //       if (err) return console.log(err)
  //     })
  //   })
  // })
  console.log('svg create')
  return stream
})
