const gulp = require('gulp')
const path = require('path')
const {reload} = require('browser-sync')
const cheerio = require('gulp-cheerio')
const replace = require('gulp-replace')
const appRoot = require('app-root-path')
const appRootBase = appRoot.path

const config = require('./config')

gulp.task('svg-prepare', () =>
  gulp
    .src(path.join(appRootBase, config.root.dist, config.svg.dist, 'svg/main.svg'))
    .pipe(cheerio({
      run: function ($) {
        // if ($('[fill]').attr('fill') === 'currentColor') {
        //   $('[fill]').removeAttr('fill')
        // }
        $('[stroke]').removeAttr('stroke')
        $('[style]').removeAttr('style')
        $('[width]').attr('width', '100%')
        $('[height]').attr('height', '100%')
        $('[y]').removeAttr('y')
      },
      parserOptions: {xmlMode: true}
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(replace(/fill="#[0-9a-f]{3,6}"/ig, ''))
    .pipe(replace(/fill="none"/ig, ''))
    .pipe(gulp.dest(path.join(appRootBase, config.root.dist, config.svg.dist, 'svg')))
    .pipe(reload({ stream: true }))
    .on('end', function () {
      console.log('svg prepare')
    }))
