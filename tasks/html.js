/**
 * Build HTML
 */
const gulp = require('gulp')
const { reload } = require('browser-sync')
const fileinclude = require('gulp-file-include')
const notify = require('gulp-notify')
const pug = require('gulp-pug')
const path = require('path')
const plumber = require('gulp-plumber')

const config = require('./config')

gulp.task('html', () =>
  gulp
    .src(path.join(config.root.dev, config.html.dev, '*.pug'))
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(pug({}))
    .pipe(gulp.dest(path.join(config.root.dist, config.html.dist)))
    .pipe(reload({ stream: true })))
