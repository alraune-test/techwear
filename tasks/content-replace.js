const gulp = require('gulp')
const path = require('path')
const fs = require('fs')
const appRoot = require('app-root-path')
const appRootBase = appRoot.path

const config = require('./config')

const pathToSprite = path.join(appRootBase, config.root.dev, config.css.dev, 'template/vendor/sprite.less')

// path fix
gulp.task('content-replace', () => {
  let stream = fs.readFile(pathToSprite, 'utf8', function (err, data) {
    if (err) {
      return console.log(err)
    }
    let result = data.replace(/dist\/assets\/images\/svg\/sprite\.svg/g, '../images/svg/sprite.svg')

    fs.writeFile(pathToSprite, result, 'utf8', function (err) {
      if (err) return console.log(err)
    })
  })
  console.log('content replace')
  return stream
})
