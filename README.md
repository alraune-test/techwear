# Gulp Webpack Starter
### Usage
1. install packages -
``bash
npm i
``
2. run dev server -
``bash
npm start
``

### Structure
* dev - develop directory
* dist - production directory
* tasks - gulp tasks

### Important
dev/less/parts is deprecated and will be removed